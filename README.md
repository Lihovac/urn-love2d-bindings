# Urn Löve2D Bindings

## Using
Clone the repository and add it to your include path by passing `-i` to the urn compiler.

## Usage Example
```common-lisp
(import love/love (defevent))
(import love/graphics)

(defevent :init ())
(defevent :draw ()
  (love/graphics/print "Hello World!" 64 64))
```
(define-native *love-events*)

(defmacro defevent (ev args &body)
  (let* [(sm (gensym))]
    `(let* [(,sm (lambda ,args ,@body))]
       (.<! *love-events* ,ev ,sm))))
